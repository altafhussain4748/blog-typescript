import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import React, { useState, createContext } from "react";
import Navbar from "./components/layouts/Navbar";
import PostList from "./components/PostList";
import CreatePost from "./components/CreatePost";
import { Provider } from "react-redux";
import store from "./redux/store";
import { User } from "./ts_types/post_ts_types.d";
const USER = {
  id: 1,
  name: "Altaf Hussain",
  userName: "Altaf.Hussain",
};
export const UserContext = React.createContext<User>(USER);

function App() {
  return (
    <UserContext.Provider value={USER}>
      <Provider store={store}>
        <div className="App">
          <Router>
            <Navbar />
            <Switch>
              <Route
                exact
                path="/"
                render={(props) => <PostList {...props} />}
              />
              <Route
                exact
                path="/create-post"
                render={(props) => <CreatePost {...props}/>}
              />
            </Switch>
          </Router>
        </div>
      </Provider>
    </UserContext.Provider>
  );
}

export default App;
