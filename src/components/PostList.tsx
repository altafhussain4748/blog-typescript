import React, { useState, useEffect } from "react";
import "./../App.css";
import LoaderComponent from "./Loader";
import { getPostState } from "../redux/selectors";
import { connect } from "react-redux";
import { fetchPosts } from "../redux";
import { Post } from "../ts_types/post_ts_types.d";
import { UserContext } from "./../App";

interface Props {
  postData: {
    isLoading: boolean;
    msg: string;
    posts: Post[];
  };
  fetchPosts: typeof fetchPosts;
}
const PostList: React.FC<Props> = ({
  postData: { isLoading, msg, posts },
  fetchPosts,
}) => {
  const userContext = React.useContext(UserContext);
  // Use hook to load data
  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div className="post-container">
      {isLoading && (
        <div className="loading">
          <LoaderComponent />
        </div>
      )}
      {msg && <p className="error">Error while loading posts</p>}
      <div className="posts-section">
        {posts.length > 0 && (
          <div className="recentPost">
            <p>Top 10 posts.</p>
          </div>
        )}
        {posts.map((post) => {
          return (
            <div key={post.id} className="single-item">
              <div className="post-top">
                <p className="post-title">{post.title}</p>
                <p className="post-user">{userContext.name}</p>
              </div>
              <p className="post-description">{post.body}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};
const makeMapStateToProps = () => {
  const makeGetPosts = getPostState();
  return (state: any) => {
    return {
      postData: makeGetPosts(state),
    };
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchPosts: () => dispatch(fetchPosts()),
  };
};

export default connect(makeMapStateToProps, mapDispatchToProps)(PostList);
