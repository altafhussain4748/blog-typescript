import React, { useState } from "react";
import "./../App.css";
import LoaderComponent from "./Loader";
import { CREATE_POST } from "../constants";
import { UserContext } from "./../App";
import { connect } from "react-redux";
import { createPosts } from "../redux";

interface Props {
  createPosts: typeof createPosts;
  isLoading: boolean;
  msg: string;
}
const CreatePost: React.FC<Props> = ({ isLoading, msg, createPosts }) => {
  const userContext = React.useContext(UserContext);
  const [state, setForm] = useState({
    title: "",
    body: "",
  });

  function handleType(
    e:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
  ) {
    setForm({ ...state, [e.target.name]: e.target.value });
  }

  async function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    const { body, title } = state;
    e.preventDefault();
    createPosts(body, title, userContext.id);
  }

  return (
    <div className="form-container">
      {isLoading && (
        <div className="loading">
          <LoaderComponent />
        </div>
      )}
      <form onSubmit={(e) => onSubmit(e)}>
        {msg && <p className="message">{msg}</p>}
        <p>Create New Post</p>
        <div className="form-group">
          <label>Title:</label>
          <input
            type="text"
            name="title"
            className="form-element"
            onChange={(e) => handleType(e)}
          />
        </div>
        <div className="form-group">
          <label>Body:</label>
          <textarea
            name="body"
            id="body"
            cols={30}
            rows={10}
            className="form-element"
            onChange={(e) => handleType(e)}
          ></textarea>
        </div>
        <input type="submit" value="Submit" className="btn" />
      </form>
    </div>
  );
};

const makeMapStateToProps = (state: any) => {
  return {
    isLoading: state.post.isLoading,
    msg: state.post.msg,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    createPosts: (body: string, title: string, userId: number) =>
      dispatch(createPosts(body, title, userId)),
  };
};

export default connect(makeMapStateToProps, mapDispatchToProps)(CreatePost);
