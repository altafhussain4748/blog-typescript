import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./../../App.css";

const Navbar = () => {
  return (
    <div>
      <nav className="navbar">
        <p>Altaf Blog</p>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/">Posts</Link>
          </li>
          <li>
            <Link to="/create-post">Add new post</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
