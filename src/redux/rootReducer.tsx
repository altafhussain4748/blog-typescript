import postReducer from "./posts/postReducer";
import { combineReducers, Reducer } from "redux";

const rootReducer = combineReducers({
  post: postReducer,
});

export default rootReducer;
