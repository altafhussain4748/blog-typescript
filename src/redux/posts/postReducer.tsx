import {
  CREATE_POSTS_REQUEST,
  CREATE_POSTS_SUCCESS,
  CREATE_POSTS_FAILURE,
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
} from "./postTypes";
import { PostState, Post } from "./../../ts_types/post_ts_types.d";

const initialState: PostState = {
  isLoading: false,
  msg: "",
  posts: [],
};

const postReducer = (
  state = initialState,
  action: { type: string; payload: string | Post[] }
) => {
  const { type, payload } = action;
  switch (type) {
    case CREATE_POSTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        msg: payload,
      };
    case CREATE_POSTS_REQUEST:
    case FETCH_POSTS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_POSTS_SUCCESS:
      return {
        isLoading: false,
        msg: "",
        posts: payload,
      };
    case CREATE_POSTS_FAILURE:
    case FETCH_POSTS_FAILURE:
      return {
        isLoading: false,
        msg: payload,
        posts: [],
      };
    default:
      return state;
  }
};

export default postReducer;
