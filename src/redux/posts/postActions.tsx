import { POST_URL, CREATE_POST } from "../../constants";
import { Post } from "./../../ts_types/post_ts_types.d";
import {
  CREATE_POSTS_REQUEST,
  CREATE_POSTS_SUCCESS,
  CREATE_POSTS_FAILURE,
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
} from "./postTypes";

export const fetchPosts = () => {
  return async (
    dispatch: (arg0: { type: string; payload?: string | Post[] }) => void
  ) => {
    try {
      dispatch(fetchPostsRequest());
      const response = await fetch(POST_URL);
      const posts = await response.json();
      dispatch(fetchPostsSuccess(posts));
    } catch (err) {
      dispatch(fetchPostsFailure(err.message));
    }
  };
};

export const createPosts = (body: string, title: string, userId: number) => {
  return async (
    dispatch: (arg0: { type: string; payload?: string }) => void
  ) => {
    try {
      dispatch(createPostsRequest());
      await fetch(CREATE_POST, {
        method: "POST",
        body: JSON.stringify({
          body,
          title,
          userId,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      dispatch(createPostsSuccess());
    } catch (err) {
      dispatch(createPostsFailure(err.message));
    }
  };
};

export const fetchPostsRequest = () => {
  return {
    type: FETCH_POSTS_REQUEST,
  };
};

export const fetchPostsSuccess = (posts: Post[]) => {
  return {
    type: FETCH_POSTS_SUCCESS,
    payload: posts,
  };
};

export const fetchPostsFailure = (error: string) => {
  return {
    type: FETCH_POSTS_FAILURE,
    payload: error,
  };
};

export const createPostsRequest = () => {
  return {
    type: CREATE_POSTS_REQUEST,
  };
};

export const createPostsSuccess = () => {
  return {
    type: CREATE_POSTS_SUCCESS,
    payload: "Post created successfully.",
  };
};

export const createPostsFailure = (error: string) => {
  return {
    type: CREATE_POSTS_FAILURE,
    payload: error,
  };
};
