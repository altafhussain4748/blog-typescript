export interface Post {
  id: number;
  title: string;
  body: string;
}

export interface User {
  id: number;
  name: string;
  userName: string;
}

export interface PostState {
  isLoading: boolean;
  msg: string;
  posts: Post[];
}
